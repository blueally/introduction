# Introduction

NetCraftsmen, a BlueAlly Company, is your partner for IT consulting and solutions. We ensure your IT infrastructure gets healthy and stays healthy by resolving current challenges, reducing risk and proactively preparing for the future.  Our clients have access to the entire NetCraftsmen team of best-in-class professionals. You’ll never get traded-down.  We empower our team of experts to do what’s right, every time.  

We specialize in serving complex, highly-regulated industries such as healthcare, energy, finance and government.  Our solutions include network, collaboration, data center, cloud, security, and other innovative technologies.

In 2022, NetCraftsmen was acquired by [BlueAlly](https://blueally.com/about), bringing a broad range of solutions, digital platforms, and manufacturer relationships. We are even better able to support our clients managed infrastructure needs and offer streamlined supply chain solutions.

Visit the NetCraftsmen public repositories at:

 * [GitHub](https://github.com/netcraftsmen)
 * [GitLab](https://gitlab.com/netcraftsmen)
 
![NetCraftsmen BlueAlly Logo](https://github.com/netcraftsmen/.github/raw/master/profile/NetCraftsmen_BlueAlly_Logo_FullColor.png)
